import { LitElement } from 'lit';

class CatalogMainDM extends LitElement {
  static get properties() {
    return {
      catalog: { type: Array },
      catalogForm: { type: Array },
      createFilm: { type: Object},
      deleteFilm: { type: String},
      detailFilm: { type: String}
    };
  }

  constructor() {
    super();

    this.getCatalog();
  }

  updated(changedProperties) {
    if (changedProperties.has('catalog')) {
      this.dispatchEvent(new CustomEvent("catalog-dm", {
        detail: {
          catalog: this.catalog
        }
      }));
    }
    
    if (changedProperties.has('catalogForm')) {
      this.getCatalog();
    }
    if (changedProperties.has('createFilm')) {
      this.requestNewFilm(this.createFilm);
    }
    if (changedProperties.has('deleteFilm')) {
      this.requestDeleteFilm(this.deleteFilm);
    }
    if (changedProperties.has('detailFilm')) {
      this.requestDetailFilm(this.detailFilm);
    }
  }

  getCatalog() {
    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
        if (xhr.status === 200) {
          console.log("peticion completada correctamente");

          let APIResponse = JSON.parse(xhr.responseText);

          console.log(APIResponse);

          this.catalog = APIResponse;
        }
    }

    xhr.open("GET", "http://localhost:8080/apitechu/v1/movies");
    xhr.send();
  }

  requestNewFilm(film){
    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
        if (xhr.status === 200) {
          console.log("Película grabada en MongoDB correctamente");
          this.dispatchEvent(new CustomEvent("save-film-dm",{ detail: film }));
        }else{
          alert("La película no ha podido grabarse correctamente");
        }
    }

    xhr.open("POST", `http://localhost:8080/apitechu/v1/movies/`);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify(film));

    //this.dispatchEvent(new CustomEvent("save-film-dm",{ detail: film }));
  }

  requestDeleteFilm(id) {
    if (id) {
      let xhr = new XMLHttpRequest();

      xhr.onload = () => {
          if (xhr.status === 200) {
            console.log("borrado completada correctamente");
            this.dispatchEvent(new CustomEvent("delete-film-dm",{ detail: id }));
          }
      }
  
      xhr.open("DELETE", `http://localhost:8080/apitechu/v1/movies/${id}`);
      xhr.send();
    } else {
      console.log(`la pelicula ${id} no puede borrarse`);
    }
    //this.dispatchEvent(new CustomEvent("delete-film-dm",{ detail: id }));
  }

  requestDetailFilm(id) {
    if(id) {
      let xhr = new XMLHttpRequest();

      xhr.onload = () => {
          if (xhr.status === 200) {
            console.log("peticion detalle completada correctamente");
  
            let APIResponse = JSON.parse(xhr.responseText);
  
            console.log(APIResponse);
  
            this.dispatchEvent(new CustomEvent("detail-film-dm",{ detail: APIResponse }));
          }
      }
  
      xhr.open("GET", `http://localhost:8080/apitechu/v1/movies/${id}`);
      xhr.send();
    } else {
      console.log(`Peticion de detalle ${id} fallida`)
    }
    //this.dispatchEvent(new CustomEvent("detail-film-dm",{ detail: film }));
  }
}

customElements.define('catalog-main-dm', CatalogMainDM);
