import { LitElement, html } from 'lit';
import './catalog-main';
import './catalog-header';
import './catalog-sidebar';

class CatalogReview extends LitElement {
  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <div>
        <catalog-header></catalog-header>
        <div class="row">
            <catalog-sidebar class="col-1" @new-film="${this.newFilm}"></catalog-sidebar>
            <catalog-main class="col-11"></catalog-main>
        </div>
      </div>
    `;
  }

  newFilm(e){
    this.shadowRoot.querySelector("catalog-main").showFilmForm = true;
  }

}

customElements.define('catalog-review', CatalogReview);
