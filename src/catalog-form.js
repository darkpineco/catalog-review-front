import { LitElement, html } from 'lit';

class CatalogForm extends LitElement {
  static get properties() {
    return{
      film: {type: Object},
      editingFilm: {type: Boolean}
    };
  }

  constructor() {
    super();
    this.resetFormData();
    this.editingFilm = false;
  }

  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <div>
            <form>
                <div class="form-group">
                    <label>Título</label>
                    <input type="text" @input="${this.updateTitle}" class="form-control" placeholder="Título de la película"
                    .value="${this.film.title}"
                    ?disabled="${this.editingFilm}"/>
                </div>
                <div class="form-group">
                    <label>Género</label>
                    <input type="text" @input="${this.updateGenre}" class="form-control" placeholder="Género de la película"
                    .value="${this.film.genre}"/>
                </div>
                <div class="form-group">
                    <label>Estreno</label>
                    <input type="text" @input="${this.updateYear}" class="form-control" placeholder="Estreno de la película"
                    .value="${this.film.year}"/>
                </div>
                <div class="form-group">
                    <label>Sinopsis</label>
                    <textarea @input="${this.updateSynopsi}" class="form-control" placeholder="Sinopsis de la película" rows="4"
                    .value="${this.film.synopsi}"></textarea>
                </div>
                <div class="form-group">
                    <label>Actores</label>
                    <textarea @input="${this.updateActors}" class="form-control" placeholder="Actores principales que aparecen en la película" rows="2"
                    .value="${this.film.actors}"></textarea>
                </div>
                <div class="d-none form-group">
                    <label>id</label>
                    <input type="text" class="form-control"
                    .value="${this.film.id}"
                    ?disabled="${this.editingFilm}"/>
                </div>
                <br />
                <button @click="${this.goBack}" class="btn btn-primary"><strong>Cancelar</strong></button>
                <button @click="${this.saveFilm}"class="btn btn-success"><strong>Guardar</strong></button>
            </form>
        </div>
    `;
  }

  saveFilm(e){
    e.preventDefault();
    this.film.photo ='../assets/default-img.png';

    let actorsArray = this.film.actors.split(",");
    let actorsList = actorsArray.map(item => {
      return {
        name: item
      }
    });

    this.dispatchEvent(new CustomEvent("film-form-save",{
        detail:{
            title: this.film.title,
            genre: this.film.genre,
            year: this.film.year,
            image: this.film.photo,
            synopsis: this.film.synopsi,
            actors: actorsList,
            editingFilm: this.editingFilm,
            film: this.film
        },
    }))
  }

  goBack(e){
    e.preventDefault();

    this.dispatchEvent(new CustomEvent("catalog-form-close",{}));
    this.resetFormData();
  }


  updateTitle(e){
    this.film.title = e.target.value;
  }

  updateGenre(e){
    this.film.genre = e.target.value;
  }

  updateYear(e){
    this.film.year = e.target.value;
  }
  updateSynopsi(e){
    this.film.synopsi = e.target.value;
  }
  updateActors(e){
    this.film.actors = e.target.value;

  }

  resetFormData(){
    this.film = {};
    this.film.title = "";
    this.film.genre = "";
    this.film.year = "";
    this.film.synopsi = "";
    this.film.actors = "";
    this.editingFilm = false;
  }
}

customElements.define('catalog-form', CatalogForm);
