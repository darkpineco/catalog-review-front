import { LitElement, html } from 'lit';

class CatalogFicha extends LitElement {
  static get properties() {
    return {
      id: {type: String},
      title: {type: String},
      image: {type: String},
      genre: {type: String},
      year: {type: Number},
      synopsis: {type: String}
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <div class="card h-100">
      <img src="${this.image}" height="350" width="200" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${this.title}</h5>
        <p class="card-text">Categoría: ${this.genre}</p>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Año: ${this.year}</li>
        </ul>
      </div>
      <div class="card-footer">
        <button class="btn btn-danger col-3" @click="${this.deleteFilm}">x</button>
        <button class="btn btn-secondary col-3 offset-1" @click="${this.editFilm}">Editar</button>
        <button class="btn btn-info col-3 offset-1" @click="${this.infoFilm}">Info</button>
      </div>
    </div>
    `;
  }

  deleteFilm(e) {
    this.dispatchEvent(new CustomEvent("delete-film", {
      detail: {
        id: this.id
      }
    }));
  }

  editFilm(e) {

  }

  infoFilm(e) {
    this.dispatchEvent(new CustomEvent("info-film", {
      detail: {
        id: this.id
      }
    }));
  }
}

customElements.define('catalog-ficha', CatalogFicha);
