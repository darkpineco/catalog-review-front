import { LitElement, html } from 'lit';
import './catalog-main-dm';
import './catalog-form';
import './catalog-ficha';
import './catalog-detail';

class CatalogMain extends LitElement {
  static get properties() {
    return {
      catalog: { type: Array },
      showFilmForm: {type: Boolean},
      showFilmDetail: {type: Boolean}
    };
  }

  constructor() {
    super();
    this.showFilmForm = false;
    this.catalog = [];
  }

  updated(changedProperties){
    console.log("updated en catalog-main");

    if (changedProperties.has("showFilmForm")){
      console.log("ha cambiado el valor de la propiedad showFilmForm en catalog-main "+this.showFilmForm);
      (this.showFilmForm === true) ? this.showFilmFormData() : this.showFilmListData();
    }
  }

  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
      <h1 class="text-center">Películas</h1>
        <div id="catalogList" class="row">
          <main class="row row-cols-1 row-cols-sm-4">
              ${this.catalog.map(item => html `<catalog-ficha
                  id="${item.id}"
                  title="${item.title}"
                  year="${item.year}"
                  genre="${item.genre}"
                  image="${item.image}"
                  @delete-film="${this.deleteFilm}"
                  @info-film="${this.showFilmDetailData}">
              </catalog-ficha>`)}
          </main>
        </div>
        <div class="row">
          <catalog-form 
          id="catalogForm" 
          @film-form-save="${this.requestNewFilm}"
          @catalog-form-close="${this.catalogFormClose}"
          class="d-none border rounded border-primary mr-4"></catalog-form>
        </div>
        <div class="row">
          <catalog-detail
            class="d-none border rounded border-primary"
            @back-to-home="${this.backToHome}"
            @get-detail="${this.getDetail}"></catalog-detail>
        </div>
        <catalog-main-dm
          @catalog-dm="${this.getCatalogDm}"
          @save-film-dm="${this.saveFilm}"
          @delete-film-dm="${this.deleteFilmDm}"
          @detail-film-dm="${this.detailFilmDm}">
        </catalog-main-dm>
    `;
  }

  getCatalogDm(e) {
    this.catalog = e.detail.catalog;
  }
  
  backToHome(e) {
    this.showFilmListData();
  }

  requestNewFilm(e){
      console.log("guardo");
      console.log(e.detail);

      this.shadowRoot.querySelector("catalog-main-dm").createFilm = e.detail;
  }

  saveFilm(e) {
    this.catalog = [...this.catalog, e.detail];
    this.shadowRoot.querySelector("catalog-main-dm").catalogForm = this.catalog;
    this.showFilmForm = false;
  }

  showFilmFormData(){
    console.log("Mostrando el formulario");
    this.shadowRoot.querySelector("catalog-detail").classList.add("d-none");
    this.shadowRoot.getElementById("catalogList").classList.add("d-none");
    this.shadowRoot.getElementById("catalogForm").classList.remove("d-none");
  }

  showFilmListData(){
    console.log("Mostrando el listado de pelis");
    this.shadowRoot.querySelector("catalog-detail").classList.add("d-none");
    this.shadowRoot.getElementById("catalogForm").classList.add("d-none");
    this.shadowRoot.getElementById("catalogList").classList.remove("d-none");
  }

  showFilmDetailData(e){
    console.log("Mostrando el detalle de pelis");
    this.getDetail(e.detail.id);
    this.shadowRoot.getElementById("catalogList").classList.add("d-none");
    this.shadowRoot.getElementById("catalogForm").classList.add("d-none");
    this.shadowRoot.querySelector("catalog-detail").classList.remove("d-none");
  }

  catalogFormClose(e){
    this.showFilmForm = false;
  }

  deleteFilm(e) {
    this.requestDeleteFilm(e.detail.id)
  }

  requestDeleteFilm(id) {
    this.shadowRoot.querySelector("catalog-main-dm").deleteFilm = id;
  }

  deleteFilmDm(e) {
    this.catalog = this.catalog.filter(
      item => item.id != e.detail
    );
  }

  getDetail(id) {
    this.shadowRoot.querySelector("catalog-main-dm").detailFilm = id;
  }

  detailFilmDm(film) {
    this.shadowRoot.querySelector("catalog-detail").film = film.detail;
  }
}

customElements.define('catalog-main', CatalogMain);