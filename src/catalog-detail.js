import { LitElement, html } from 'lit';

class CatalogDetail extends LitElement {
  static get properties() {
    return {
      film: {type: Object}
    };
  }

  constructor() {
    super();

    this.film ={
      actors:[
      ]
    }
;   
  }

  updated(changedProperties) {
  }

  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <div class="row">
    <div class="col-2">
    <img src="${this.film.image}" alt="Generic placeholder image">
    </div>
    <div class="col-9">
    <div class="media-body">
    <h5 class="mt-0">${this.film.title}</h5>
    <p><b>Sinopsis:</b> ${this.film.synopsis}</p>
    <p><b>Genero:</b> ${this.film.genre}</p>
    <p><b>Año:</b> ${this.film.year}</p>
    <p><b>Actores:</b>  ${this.film.actors.map(item => html `${item.name}, `)}</p>

  </div>
</div>
      
      <div class="card-footer">
       <button class="btn btn-dark col-12" @click="${this.backToHome}">Atras</button>
      </div>
    </div>
    `;
  }

  backToHome(e) {
    this.dispatchEvent(new CustomEvent("back-to-home", {
      detail: {
        id: this.id
      }
    }));
  }
}

customElements.define('catalog-detail', CatalogDetail);
