import { LitElement, html } from 'lit';

class CatalogSidebar extends LitElement {
  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <aside>
      <section>
        <div class="mt-5">
          <button
            class="btn btn-success w-100 offset-1 mt-4" @click="${this.newFilm}"><b style="font-size: 50px">+</b></button>
        </div>
      </section>
    </aside>
    `;
  }

  newFilm(e){
    console.log("se va a crear una nueva película");

    this.dispatchEvent(new CustomEvent("new-film",{}));
}
}

customElements.define('catalog-sidebar', CatalogSidebar);
